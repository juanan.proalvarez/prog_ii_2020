def f(x: float) -> float:
    return 4 * x ** 2 + 3


def g(x: float) -> float:
    return 2 * x + 1


print(f(g(3)))
