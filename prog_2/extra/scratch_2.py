import logging
logging.basicConfig()
logger = logging.getLogger('function_logger')
logger.setLevel(logging.WARNING)

file_handler = logging.FileHandler('/Users/rlucerga/Dropbox (Personal)/Innitium/Clients/UFV/PII/2021/06 Documents/prog_ufv/prog_2/detailed.log')
formatter = logging.Formatter('%(asctime)s - %(funcName)s - %(message)s')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

def f(x):
    logger.info('elevo al cuadrado')
    try:
        u = x ** 2
    except TypeError:
        logger.error('wrong type')
    else:
        return u


print(f('a'),'\n')