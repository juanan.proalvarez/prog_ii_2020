"""
This gives an error as c is not defined in the scope of add.
"""

c = 1  # global variable


def add():
    c = c + 2  # increment c by 2
    print(c)


add()
