import concurrent.futures
import time

start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    time.sleep(seconds)
    return f'Done Sleeping...{seconds}'


if __name__ == '__main__':
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        # Encapsulates the asynchronous execution of a callable. Future instances are created by
        # Executor.submit()
        results = [executor.submit(do_something, 1) for _ in range(10)]

        # Returns an iterator over the Future instances (possibly created by different Executor
        # instances) given by fs that yields futures as they complete (finished or cancelled
        # futures).
        for f in concurrent.futures.as_completed(results):
            print(f.result())

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')
