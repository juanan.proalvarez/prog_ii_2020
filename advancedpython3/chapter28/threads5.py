from threading import Thread
from time import sleep


def worker(msg):
    for i in range(0, 10):
        print(msg, end='', flush=True)
        sleep(1)


print('Starting')
# Create a daemon thread
d = Thread(name= 'daemon', daemon=True, target=worker, args='C')
# d = Thread(name= 'non-daemon', daemon=False, target=worker, args='C')
d.start()

sleep(5)
print('Done')