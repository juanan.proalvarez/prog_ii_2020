import logging.config
import yaml

with open('logging.config.yaml', 'r') as f:
    config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)

logger = logging.getLogger('myLogger')

def do_something():
    logger.debug('debug message')

logger.info('Starting')
do_something()
logger.info('Done')
