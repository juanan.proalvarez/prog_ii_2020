from typing import Optional
from email_validator import EmailNotValidError, validate_email

class User:
    # we create the class User
    def __init__(self, name: str, email: str = None):
        """
        :param name: the name of the user
        :param email: the email of this user that is optional
        """
        # as the email is optional , if the if the user writes the email, it will validated and
        # if it doesnt if the user writes the email, we will call our static.method to validate
        # the email, we will assing what our method returns to the variable email
        self.name = name
        self.email = None
        if email is not None:
            self.email = self.validator(email)

    def __str__(self):
        """
        :return: prints out the user and the email
        """
        return f'The User{self.name}, email: {self.email}'

    @staticmethod
    def validator(email: str) -> Optional[str]:
        """
        :param email:the email that we want to check
        :return: if it isnt valid
        """
        try:
            # if the email it valid we return the email
            validate_email(email)
            return email
        except EmailNotValidError:
            # it the email isnt valid we print the mensaje and return None
            print('The Email isn`t valid')
            return None


user_1 = User('Ester', 'esterarvillagras@ufv.es')
user_2 = User('Bob', 'this@isabademail')