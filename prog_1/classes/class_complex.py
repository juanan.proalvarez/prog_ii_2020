class Complex:
    def __init__(self, realpart: float = 0.0, imagpart: float = 0.0):
        """
        Complex number constructor
        :param realpart: real part of the complex number
        :param imagpart: imaginary part of the complex number
        """
        self.r = realpart
        self.i = imagpart

    def absolute(self):
        """
        Computes absolute value of the complex number
        :return: the absolute value
        """
        return (self.r ** 2 + self.i ** 2) ** 0.5


x = Complex(3.0, -4.5)
print(x)
print(x.r, x.i)
