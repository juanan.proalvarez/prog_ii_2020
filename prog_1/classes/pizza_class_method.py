# https://realpython.com/instance-class-and-static-methods-demystified/

class Pizza:
    def __init__(self, ingredients: list):
        self.ingredients = ingredients

    def __repr__(self):
        return f'Pizza({self.ingredients})'

    @classmethod
    def margherita(cls):
        return cls(['mozzarella', 'tomatoes'])

    @classmethod
    def prosciutto(cls):
        return cls(['mozzarella', 'tomatoes', 'ham'])


Pizza.margherita()
Pizza.prosciutto()
