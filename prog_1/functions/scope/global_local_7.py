# This creates an exception, as a is defined as a local variable, but the expresion
# a_variable + 7 cannot be resolved, because a_variable is not yet defined in the
# local namespace

def outer():
    def inner():
        # a_variable = a_variable + 7
        print(a_variable)

    a_variable = 2
    print(a_variable)
    inner()
    print(a_variable)


a_variable = 10
outer()
print(a_variable)
