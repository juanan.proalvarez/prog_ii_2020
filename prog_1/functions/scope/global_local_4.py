# We specify nonlocal, Python binds inner's local_var with local variabe in outer scope
# in this caste outer

def outer():
    def inner():
        nonlocal local_var
        local_var = 7
        print(local_var)

    local_var = 2
    print(local_var)
    inner()
    print(local_var)


outer()
