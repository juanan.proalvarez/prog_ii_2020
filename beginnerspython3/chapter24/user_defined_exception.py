class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class InputError(Error):
    """
    Exception raised for errors in the input.
    """

    def __init__(self, expression, message):
        """
        :param expression:  input expression in which the error occurred
        :param message:     explanation of the error
        """
        self.expression = expression
        self.message = message

class TransitionError(Error):
    """
    Raised when an operation attempts a state transition that's not
    allowed.
    """
    def __init__(self, previous, next, message):
        """

        :param previous:    state at beginning of transition
        :param next:        attempted new state
        :param message:     explanation of why the specific transition is not allowed
        """
        self.previous = previous
        self.next = next
        self.message = message
