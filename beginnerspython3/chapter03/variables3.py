# Can check the type of a variable

my_variable = 42
print(my_variable)
print(type(my_variable))

my_variable = 'John'
print(type(my_variable))

my_variable = True
print(type(my_variable))