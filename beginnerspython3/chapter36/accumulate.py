import operator
from itertools import accumulate

data = [3, 4, 6, 2, 1, 9, 0, 7, 5, 8]
l1 = list(accumulate(data, operator.mul))     # running product
print(l1)

l2 = list(accumulate(data, max))              # running maximum
print(l2)

cashflows = [1000, -90, -90, -90, -90]
l3 = list(accumulate(cashflows, lambda bal, pmt: bal*1.05 + pmt))
print(l3)