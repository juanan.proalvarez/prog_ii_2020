class Employee:
    """
    Un empleado
    """
    instance_count = 0

    @classmethod
    def increment_instance_count(cls):
        cls.instance_count += 1

    def __init__(self, name, age):
        """
        Constructor
        :param name:    name of the employee
        :param age:     age
        """
        Employee.increment_instance_count()
        self.name = name
        self.age = age


p1 = Employee('Jason', 36)
print(Employee.instance_count)
p2 = Employee('Carol', 21)
print(Employee.instance_count)
Employee.increment_instance_count()
print(Employee.instance_count)
