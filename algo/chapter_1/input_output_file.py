# Read write example to file

"""
To write to an existing file, you must add a parameter to the open() function:
"a" - Append - will append to the end of the file
"w" - Write - will overwrite any existing content
"""

with open("demofile.txt", "w") as f:
    f.write("New content!\n")

f = open("demofile.txt", "a")
f.write("New content!")
f.close()

# open and read the file after the appending:
f = open("demofile.txt", "r")
print(f.read())

f = open("demofile3.txt", "w")
f.write("Woops! I have deleted the content!")
f.close()

# open and read the file after the appending:
f = open("demofile3.txt", "r")
print(f.read())

# Example taken from https://www.w3schools.com/python/python_file_write.asp
